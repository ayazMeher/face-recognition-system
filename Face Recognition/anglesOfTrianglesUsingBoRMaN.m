function [ angles ] = anglesOfTrianglesUsingBoRMaN( pts )

%all posible sides of lips
[ab] = distanceCal(pts(15),pts(16));
[ac] = distanceCal(pts(15),pts(17));
[ad] = distanceCal(pts(15),pts(18));
[bc] = distanceCal(pts(16),pts(17));
[bd] = distanceCal(pts(16),pts(18));
[cd] = distanceCal(pts(17),pts(18));
%all posible sides of left eye
[ef] = distanceCal(pts(1),pts(3));
[eg] = distanceCal(pts(1),pts(9));
[eh] = distanceCal(pts(1),pts(11));
[fg] = distanceCal(pts(3),pts(9));
[fh] = distanceCal(pts(3),pts(11));
[gh] = distanceCal(pts(9),pts(11));
%all posible sides of right eye
[ij] = distanceCal(pts(4),pts(2));
[ik] = distanceCal(pts(4),pts(10));
[il] = distanceCal(pts(4),pts(12));
[jk] = distanceCal(pts(2),pts(10));
[jl] = distanceCal(pts(2),pts(12));
[kl] = distanceCal(pts(10),pts(12));
%all posible sides of nose
[mn] = distanceCal(pts(20),pts(13));
[mo] = distanceCal(pts(20),pts(14));
[mp] = distanceCal(pts(20),pts(17));
[no] = distanceCal(pts(13),pts(14));
[np] = distanceCal(pts(13),pts(17));
[op] = distanceCal(pts(14),pts(17));

%sides of big triangles

[eler] = distanceCal(pts(21),pts(22));
[eln] = distanceCal(pts(21),pts(20));
[ern] = distanceCal(pts(22),pts(20));

[ell] = distanceCal(pts(21),pts(18));
[erl] = distanceCal(pts(22),pts(18));

[elm] = distanceCal(pts(21),pts(19));
[erm] = distanceCal(pts(22),pts(19));

[ab1] = distanceCal(pts(1),pts(4));
[ah] = distanceCal(pts(1),pts(13));
[b1h] = distanceCal(pts(4),pts(13));

[a1b] = distanceCal(pts(2),pts(3));
[a1h1] = distanceCal(pts(2),pts(14));
[bh1] = distanceCal(pts(3),pts(14));

[ed1] = distanceCal(pts(7),pts(6));
[ei] = distanceCal(pts(7),pts(15));
[d1i] = distanceCal(pts(6),pts(15));

[e1d] = distanceCal(pts(8),pts(5));
[dj] = distanceCal(pts(5),pts(16));
[e1j] = distanceCal(pts(8),pts(16));

[ee1] = distanceCal(pts(7),pts(8));
[em] = distanceCal(pts(7),pts(19));
[e1m] = distanceCal(pts(8),pts(19));

%lips
[ abc_a,abc_b,abc_g] = angleCal( ab,ac,bc );
[ abd_a,abd_b,abd_g] = angleCal( ab,ad,bd );
[ acd_a,acd_b,acd_g] = angleCal( ac,ad,cd );
[ bcd_a,bcd_b,bcd_g] = angleCal( bc,bd,cd );

angles(1,1)=abc_a;
angles(2,1)=abc_b;
angles(3,1)=abc_g;

angles(4,1)=abd_a;
angles(5,1)=abd_b;
angles(6,1)=abd_g;

angles(7,1)=acd_a;
angles(8,1)=acd_b;
angles(9,1)=acd_g;

angles(10,1)=bcd_a;
angles(11,1)=bcd_b;
angles(12,1)=bcd_g;
%Left eye
[ efg_a,efg_b,efg_g] = angleCal( ef,eg,fg );
[ efh_a,efh_b,efh_g] = angleCal( ef,eh,fh );
[ egh_a,egh_b,egh_g] = angleCal( eg,eh,gh );
[ fgh_a,fgh_b,fgh_g] = angleCal( fg,fh,gh );

angles(13,1)=efg_a;
angles(14,1)=efg_b;
angles(15,1)=efg_g;

angles(16,1)=efh_a;
angles(17,1)=efh_b;
angles(18,1)=efh_g;

angles(19,1)=egh_a;
angles(20,1)=egh_b;
angles(21,1)=egh_g;

angles(22,1)=fgh_a;
angles(23,1)=fgh_b;
angles(24,1)=fgh_g;
%right eye
[ ijk_a,ijk_b,ijk_g] = angleCal( ij,ik,jk );
[ ijl_a,ijl_b,ijl_g] = angleCal( ij,il,jl );
[ ikl_a,ikl_b,ikl_g] = angleCal( ik,il,kl );
[ jkl_a,jkl_b,jkl_g] = angleCal( jk,jl,kl );

angles(25,1)=ijk_a;
angles(26,1)=ijk_b;
angles(27,1)=ijk_g;

angles(28,1)=ijl_a;
angles(29,1)=ijl_b;
angles(30,1)=ijl_g;

angles(31,1)=ikl_a;
angles(32,1)=ikl_b;
angles(33,1)=ikl_g;

angles(34,1)=jkl_a;
angles(35,1)=jkl_b;
angles(36,1)=jkl_g;

%nose
[ mno_a,mno_b,mno_g] = angleCal( mn,mo,no );
[ mnp_a,mnp_b,mnp_g] = angleCal( mn,mp,np );
[ mop_a,mop_b,mop_g] = angleCal( mo,mp,op );
[ nop_a,nop_b,nop_g] = angleCal( no,np,op );

angles(37,1)=mno_a;
angles(38,1)=mno_b;
angles(39,1)=mno_g;

angles(40,1)=mnp_a;
angles(41,1)=mnp_b;
angles(42,1)=mnp_g;

angles(43,1)=mop_a;
angles(44,1)=mop_b;
angles(45,1)=mop_g;

angles(46,1)=nop_a;
angles(47,1)=nop_b;
angles(48,1)=nop_g;
% big triangles
[ a_a,a_b,a_g] = angleCal(eler,eln,ern );
[ b_a,b_b,b_g] = angleCal( eler,ell,erl );
[ c_a,c_b,c_g] = angleCal( eler,elm,erm );
[ d_a,d_b,d_g] = angleCal( ab1,ah,b1h );
[ e_a,e_b,e_g] = angleCal( a1b,a1h1,bh1 );
[ f_a,f_b,f_g] = angleCal( ed1,ei,d1i );
[ g_a,g_b,g_g] = angleCal( e1d,dj,e1j );
[ h_a,h_b,h_g] = angleCal( ee1,em,e1m );
%triangle 1
angles(49,1)=a_a;
angles(50,1)=a_b;
angles(51,1)=a_g;
%triangle 2
angles(52,1)=b_a;
angles(53,1)=b_b;
angles(54,1)=b_g;
%triangle 3
angles(55,1)=c_a;
angles(56,1)=c_b;
angles(57,1)=c_g;
%triangle 4
angles(58,1)=d_a;
angles(59,1)=d_b;
angles(60,1)=d_g;
%triangle 5
angles(61,1)=e_a;
angles(62,1)=e_b;
angles(63,1)=e_g;
%triangle 6
angles(64,1)=f_a;
angles(65,1)=f_b;
angles(66,1)=f_g;
%triangle 7
angles(67,1)=g_a;
angles(68,1)=g_b;
angles(69,1)=g_g;
%triangle 8
angles(70,1)=h_a;
angles(71,1)=h_b;
angles(72,1)=h_g;
end

