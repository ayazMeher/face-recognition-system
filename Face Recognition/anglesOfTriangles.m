function [ angles ] = anglesOfTriangles( pts )








%all posible sides of lips
[ab] = distanceCal(pts(1),pts(2));
[ac] = distanceCal(pts(1),pts(3));
[ad] = distanceCal(pts(1),pts(4));
[bc] = distanceCal(pts(2),pts(3));
[bd] = distanceCal(pts(2),pts(4));
[cd] = distanceCal(pts(3),pts(4));
%all posible sides of left eye
[ef] = distanceCal(pts(5),pts(6));
[eg] = distanceCal(pts(5),pts(7));
[eh] = distanceCal(pts(5),pts(8));
[fg] = distanceCal(pts(6),pts(7));
[fh] = distanceCal(pts(6),pts(8));
[gh] = distanceCal(pts(7),pts(8));
%all posible sides of right eye
[ij] = distanceCal(pts(9),pts(10));
[ik] = distanceCal(pts(9),pts(11));
[il] = distanceCal(pts(9),pts(12));
[jk] = distanceCal(pts(10),pts(11));
[jl] = distanceCal(pts(10),pts(12));
[kl] = distanceCal(pts(11),pts(12));
%all posible sides of nose
[mn] = distanceCal(pts(13),pts(14));
[mo] = distanceCal(pts(13),pts(15));
[mp] = distanceCal(pts(13),pts(16));
[no] = distanceCal(pts(14),pts(15));
[np] = distanceCal(pts(14),pts(16));
[op] = distanceCal(pts(15),pts(16));
%lips
[ abc_a,abc_b,abc_g] = angleCal( ab,ac,bc );
[ abd_a,abd_b,abd_g] = angleCal( ab,ad,bd );
[ acd_a,acd_b,acd_g] = angleCal( ac,ad,cd );
[ bcd_a,bcd_b,bcd_g] = angleCal( bc,bd,cd );

angles(1,1)=abc_a;
angles(2,1)=abc_b;
angles(3,1)=abc_g;

angles(4,1)=abd_a;
angles(5,1)=abd_b;
angles(6,1)=abd_g;

angles(7,1)=acd_a;
angles(8,1)=acd_b;
angles(9,1)=acd_g;

angles(10,1)=bcd_a;
angles(11,1)=bcd_b;
angles(12,1)=bcd_g;
%Left eye
[ efg_a,efg_b,efg_g] = angleCal( ef,eg,fg );
[ efh_a,efh_b,efh_g] = angleCal( ef,eh,fh );
[ egh_a,egh_b,egh_g] = angleCal( eg,eh,gh );
[ fgh_a,fgh_b,fgh_g] = angleCal( fg,fh,gh );

angles(13,1)=efg_a;
angles(14,1)=efg_b;
angles(15,1)=efg_g;

angles(16,1)=efh_a;
angles(17,1)=efh_b;
angles(18,1)=efh_g;

angles(19,1)=egh_a;
angles(20,1)=egh_b;
angles(21,1)=egh_g;

angles(22,1)=fgh_a;
angles(23,1)=fgh_b;
angles(24,1)=fgh_g;
%right eye
[ ijk_a,ijk_b,ijk_g] = angleCal( ij,ik,jk );
[ ijl_a,ijl_b,ijl_g] = angleCal( ij,il,jl );
[ ikl_a,ikl_b,ikl_g] = angleCal( ik,il,kl );
[ jkl_a,jkl_b,jkl_g] = angleCal( jk,jl,kl );

angles(25,1)=ijk_a;
angles(26,1)=ijk_b;
angles(27,1)=ijk_g;

angles(28,1)=ijl_a;
angles(29,1)=ijl_b;
angles(30,1)=ijl_g;

angles(31,1)=ikl_a;
angles(32,1)=ikl_b;
angles(33,1)=ikl_g;

angles(34,1)=jkl_a;
angles(35,1)=jkl_b;
angles(36,1)=jkl_g;

%nose
[ mno_a,mno_b,mno_g] = angleCal( mn,mo,no );
[ mnp_a,mnp_b,mnp_g] = angleCal( mn,mp,np );
[ mop_a,mop_b,mop_g] = angleCal( mo,mp,op );
[ nop_a,nop_b,nop_g] = angleCal( no,np,op );

angles(37,1)=mno_a;
angles(38,1)=mno_b;
angles(39,1)=mno_g;

angles(40,1)=mnp_a;
angles(41,1)=mnp_b;
angles(42,1)=mnp_g;

angles(43,1)=mop_a;
angles(44,1)=mop_b;
angles(45,1)=mop_g;

angles(46,1)=nop_a;
angles(47,1)=nop_b;
angles(48,1)=nop_g;

end

