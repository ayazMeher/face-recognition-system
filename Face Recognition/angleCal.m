function [ alpha,beta,gamma] = angleCal( a,b,c )

cos_alpha=(b^2+c^2-a^2)/(2*b*c);
cos_beta=(c^2+a^2+-b^2)/(2*c*a);
cos_gamma=(a^2+b^2-c^2)/(2*a*b);

alpha=acosd(cos_alpha);
beta=acosd(cos_beta);
gamma=acosd(cos_gamma);

end

