function [ Pts ] = ASMPtsCal(img)

%  tic
%  close all;
%  clear all;


detector = buildDetector();

[bbox bbimg faces bbfaces] = detectFaceParts(detector,img,2);

figure(1);

imshow(bbimg);

figure(22)
cellplot(faces);

for i=1:size(bbfaces,1)
  figure(2);imshow(bbfaces{i});
end



mouthx1=bbox(1,13);
mouthx2=bbox(1,15)+mouthx1;
mouthy1=bbox(1,14);
mouthy2=bbox(1,16)+mouthy1;

nosex1=bbox(1,17);
nosex2=bbox(1,19)+nosex1;
nosey1=bbox(1,18);
nosey2=bbox(1,20)+nosey1;

eyeLx1=bbox(1,5);
eyeLx2=bbox(1,7)+eyeLx1;
eyeLy1=bbox(1,6);
eyeLy2=bbox(1,8)+eyeLy1;

eyeRx1=bbox(1,9);
eyeRx2=bbox(1,11)+eyeRx1;
eyeRy1=bbox(1,10);
eyeRy2=bbox(1,12)+eyeRy1;

mouthPoint1 = [ mouthx1 (mouthy1+mouthy2)/2 ];
mouthPoint2 = [ mouthx2 (mouthy1+mouthy2)/2 ];
mouthPoint3 = [ (mouthx1+mouthx2)/2 mouthy1+10 ];
mouthPoint4 = [ (mouthx1+mouthx2)/2 mouthy2-10 ];

eyeLPoint1 = [ eyeLx1+10 (eyeLy1+eyeLy2)/2 ];
eyeLPoint2 = [ eyeLx2-10 (eyeLy1+eyeLy2)/2 ];
eyeLPoint3 = [ (eyeLx1+eyeLx2)/2 eyeLy1+10 ];
eyeLPoint4 = [ (eyeLx1+eyeLx2)/2 eyeLy2-10 ];

eyeRPoint1 = [ eyeRx1+10 (eyeRy1+eyeRy2)/2 ];
eyeRPoint2 = [ eyeRx2-10 (eyeRy1+eyeRy2)/2 ];
eyeRPoint3 = [ (eyeRx1+eyeRx2)/2 eyeRy1+10 ];
eyeRPoint4 = [ (eyeRx1+eyeRx2)/2 eyeRy2-10 ];

nosePoint1 = [ nosex1 (nosey1+nosey2)/2 ];
nosePoint2 = [ nosex2 (nosey1+nosey2)/2 ];
nosePoint3 = [ (nosex1+nosex2)/2 nosey1+10 ];
nosePoint4 = [ (nosex1+nosex2)/2 nosey2-10 ];

points = [mouthPoint1 ; mouthPoint2 ; mouthPoint3 ; mouthPoint4 ; eyeLPoint1 ; eyeLPoint2 ; eyeLPoint3 ; eyeLPoint4 ];
points = [points ; eyeRPoint1 ; eyeRPoint2 ; eyeRPoint3 ; eyeRPoint4 ; nosePoint1 ; nosePoint2 ; nosePoint3 ; nosePoint4 ];

markerInserter = vision.MarkerInserter('Shape','X-mark','BorderColor','white');
%Pts = int32([10 10; 20 20; 30 30]);
Pts = int32(points);


% J = step(markerInserter, img, Pts);
% imshow(J);

% imwrite(J,'new.jpg');


c=5;


Pts(1,1)=Pts(1,1)+c;
Pts(1,2)=Pts(1,2);
Pts(2,1)=Pts(2,1)-c;
Pts(2,2)=Pts(2,2);

Pts(3,1)=Pts(3,1);
Pts(3,2)=(Pts(3,2)+c)-3;
Pts(4,1)=Pts(4,1);
Pts(4,2)=3+(Pts(4,2)-c);


Pts(5,1)=Pts(5,1)+c;
Pts(5,2)=Pts(5,2);
Pts(6,1)=Pts(6,1)-c;
Pts(6,2)=Pts(6,2);

Pts(7,1)=Pts(7,1);
Pts(7,2)=Pts(7,2)+c;
Pts(8,1)=Pts(8,1);
Pts(8,2)=Pts(8,2)-c;


Pts(9,1)=Pts(9,1)+c;
Pts(9,2)=Pts(9,2);
Pts(10,1)=Pts(10,1)-c;
Pts(10,2)=Pts(10,2);

Pts(11,1)=Pts(11,1);
Pts(11,2)=Pts(11,2)+c;
Pts(12,1)=Pts(12,1);
Pts(12,2)=Pts(12,2)-c;



Pts(13,1)=Pts(13,1)+c;
Pts(13,2)=Pts(13,2);
Pts(14,1)=Pts(14,1)-c;
Pts(14,2)=Pts(14,2);


Pts(15,1)=Pts(15,1);
Pts(15,2)=Pts(15,2);
Pts(16,1)=Pts(16,1);
Pts(16,2)=2+(Pts(16,2)-c);



Jj = step(markerInserter, img, Pts);
figure(3)


imshow(Jj);
impixelinfo
imwrite(Jj,'newj.jpg');
% time=toc;
% disp(time)
end

