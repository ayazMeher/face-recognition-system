function [ ASMFeatures ] = DriverASM( Pts )

ASMFeatures=zeros(188,1);
 c=1;
 
 [pts1]= pointCal(Pts);
 [ ratiosASM ] = ratios( pts1 );
 [ areasASM ] = areasOfTriangles(pts1);
 [ anglesASM ] = anglesOfTriangles( pts1 );
 [ lengtsASM ] = lengths( pts1 );
 
 for i=1:size(ratiosASM,1)
     ASMFeatures(c,1)=ratiosASM(i,1);
     c=c+1;
 end
 
 for i=1:size(areasASM,1)
     ASMFeatures(c,1)=areasASM(i,1);
     c=c+1;
 end
 for i=1:size(anglesASM,1)
     ASMFeatures(c,1)=anglesASM(i,1);
     c=c+1;
 end
for i=1:size(lengtsASM,1)
     ASMFeatures(c,1)=lengtsASM(i,1);
     c=c+1;
end
end