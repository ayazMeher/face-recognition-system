function [ areas ] = areasOfTriangles( pts)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

areas=zeros(4,1);


%all posible sides of lips
[ab] = distanceCal(pts(1),pts(2));
[ac] = distanceCal(pts(1),pts(3));
[ad] = distanceCal(pts(1),pts(4));
[bc] = distanceCal(pts(2),pts(3));
[bd] = distanceCal(pts(2),pts(4));
[cd] = distanceCal(pts(3),pts(4));
%all posible sides of left eye
[ef] = distanceCal(pts(5),pts(6));
[eg] = distanceCal(pts(5),pts(7));
[eh] = distanceCal(pts(5),pts(8));
[fg] = distanceCal(pts(6),pts(7));
[fh] = distanceCal(pts(6),pts(8));
[gh] = distanceCal(pts(7),pts(8));
%all posible sides of right eye
[ij] = distanceCal(pts(9),pts(10));
[ik] = distanceCal(pts(9),pts(11));
[il] = distanceCal(pts(9),pts(12));
[jk] = distanceCal(pts(10),pts(11));
[jl] = distanceCal(pts(10),pts(12));
[kl] = distanceCal(pts(11),pts(12));
%all posible sides of nose
[mn] = distanceCal(pts(13),pts(14));
[mo] = distanceCal(pts(13),pts(15));
[mp] = distanceCal(pts(13),pts(16));
[no] = distanceCal(pts(14),pts(15));
[np] = distanceCal(pts(14),pts(16));
[op] = distanceCal(pts(15),pts(16));





%all posibles triangle of Lips
[abc] = AreaCal( ab,ac,bc );
[abd] = AreaCal( ab,ad,bd );
[acd] = AreaCal( ac,ad,cd );
[bcd] = AreaCal( bc,bd,cd );
%all posibles triangle of Left eye
[efg] = AreaCal( ef,eg,fg );
[efh] = AreaCal( ef,eh,fh );
[egh] = AreaCal( eg,eh,gh );
[fgh] = AreaCal( fg,fh,gh );
%all posibles triangle of right eye
[ijk] = AreaCal( ij,ik,jk );
[ijl] = AreaCal( ij,il,jl );
[ikl] = AreaCal( ik,il,kl );
[jkl] = AreaCal( jk,jl,kl );
%all posibles triangle of nose
[mno] = AreaCal( mn,mo,no );
[mnp] = AreaCal( mn,mp,np );
[mop] = AreaCal( mo,mp,op );
[nop] = AreaCal( no,np,op );

%area of Lips
areas(1,1)=abc;
areas(2,1)=abd;
areas(3,1)=acd;
areas(4,1)=bcd;
%area of Left eye
areas(5,1)=efg;
areas(6,1)=efh;
areas(7,1)=egh;
areas(8,1)=fgh;
%area of right eye
areas(9,1)=ijk;
areas(10,1)=ijl;
areas(11,1)=ikl;
areas(12,1)=jkl;
%area of nose
areas(13,1)=mno;
areas(14,1)=mnp;
areas(15,1)=mop;
areas(16,1)=nop;



end

