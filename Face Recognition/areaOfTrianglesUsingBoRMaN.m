function [ areas ] =areaOfTrianglesUsingBoRMaN( pts )

areas=zeros(24,1);


%all posible sides of lips
[ab] = distanceCal(pts(15),pts(16));
[ac] = distanceCal(pts(15),pts(17));
[ad] = distanceCal(pts(15),pts(18));
[bc] = distanceCal(pts(16),pts(17));
[bd] = distanceCal(pts(16),pts(18));
[cd] = distanceCal(pts(17),pts(18));
%all posible sides of left eye
[ef] = distanceCal(pts(1),pts(3));
[eg] = distanceCal(pts(1),pts(9));
[eh] = distanceCal(pts(1),pts(11));
[fg] = distanceCal(pts(3),pts(9));
[fh] = distanceCal(pts(3),pts(11));
[gh] = distanceCal(pts(9),pts(11));
%all posible sides of right eye
[ij] = distanceCal(pts(4),pts(2));
[ik] = distanceCal(pts(4),pts(10));
[il] = distanceCal(pts(4),pts(12));
[jk] = distanceCal(pts(2),pts(10));
[jl] = distanceCal(pts(2),pts(12));
[kl] = distanceCal(pts(10),pts(12));
%all posible sides of nose
[mn] = distanceCal(pts(20),pts(13));
[mo] = distanceCal(pts(20),pts(14));
[mp] = distanceCal(pts(20),pts(17));
[no] = distanceCal(pts(13),pts(14));
[np] = distanceCal(pts(13),pts(17));
[op] = distanceCal(pts(14),pts(17));





%all posibles triangle of Lips
[abc] = AreaCal( ab,ac,bc );
[abd] = AreaCal( ab,ad,bd );
[acd] = AreaCal( ac,ad,cd );
[bcd] = AreaCal( bc,bd,cd );
%all posibles triangle of Left eye
[efg] = AreaCal( ef,eg,fg );
[efh] = AreaCal( ef,eh,fh );
[egh] = AreaCal( eg,eh,gh );
[fgh] = AreaCal( fg,fh,gh );
%all posibles triangle of right eye
[ijk] = AreaCal( ij,ik,jk );
[ijl] = AreaCal( ij,il,jl );
[ikl] = AreaCal( ik,il,kl );
[jkl] = AreaCal( jk,jl,kl );
%all posibles triangle of nose
[mno] = AreaCal( mn,mo,mp );
[mnp] = AreaCal( mn,mp,np );
[mop] = AreaCal( mo,mp,op );
[nop] = AreaCal( no,np,op );



%sides of big triangles

[eler] = distanceCal(pts(21),pts(22));
[eln] = distanceCal(pts(21),pts(20));
[ern] = distanceCal(pts(22),pts(20));

[ell] = distanceCal(pts(21),pts(18));
[erl] = distanceCal(pts(22),pts(18));

[elm] = distanceCal(pts(21),pts(19));
[erm] = distanceCal(pts(22),pts(19));

[ab1] = distanceCal(pts(1),pts(4));
[ah] = distanceCal(pts(1),pts(13));
[b1h] = distanceCal(pts(4),pts(13));

[a1b] = distanceCal(pts(2),pts(3));
[a1h1] = distanceCal(pts(2),pts(14));
[bh1] = distanceCal(pts(3),pts(14));

[ed1] = distanceCal(pts(7),pts(6));
[ei] = distanceCal(pts(7),pts(15));
[d1i] = distanceCal(pts(6),pts(15));

[e1d] = distanceCal(pts(8),pts(5));
[dj] = distanceCal(pts(5),pts(16));
[e1j] = distanceCal(pts(8),pts(16));

[ee1] = distanceCal(pts(7),pts(8));
[em] = distanceCal(pts(7),pts(19));
[e1m] = distanceCal(pts(8),pts(19));

[a] = AreaCal( eler,eln,ern );
[b] = AreaCal( eler,ell,erl );
[c] = AreaCal( eler,elm,erm );
[d] = AreaCal( ab1,ah,b1h );
[e] = AreaCal( a1b,a1h1,bh1 );
[f] = AreaCal( ed1,ei,d1i );
[g] = AreaCal( e1d,dj,e1j );
[h] = AreaCal( ee1,em,e1m );

%area of Lips
areas(1,1)=abc;
areas(2,1)=abd;
areas(3,1)=acd;
areas(4,1)=bcd;
%area of Left eye
areas(5,1)=efg;
areas(6,1)=efh;
areas(7,1)=egh;
areas(8,1)=fgh;
%area of right eye
areas(9,1)=ijk;
areas(10,1)=ijl;
areas(11,1)=ikl;
areas(12,1)=jkl;
%area of nose
areas(13,1)=mno;
areas(14,1)=mnp;
areas(15,1)=mop;
areas(16,1)=nop;

% area of big triangles 
areas(17,1)=a;
areas(18,1)=b;
areas(19,1)=c;
areas(20,1)=d;

areas(21,1)=e;
areas(22,1)=f;
areas(23,1)=g;
areas(24,1)=h;

end

