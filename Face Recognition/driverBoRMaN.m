function [BoRMaNFeatures ] = driverBoRMaN( PtsBoRMaN )



BoRMaNFeatures=zeros(331,1);
 c=1;
 
 [ pts2 ] = BoRMaNptsCal(PtsBoRMaN);
 [ ratiosBoRMaN ] = ratiosUsingBoRMaN( pts2 );
 [ areasBoRMaN ] =areaOfTrianglesUsingBoRMaN( pts2 );
 [ anglesBoRMaN ] = anglesOfTrianglesUsingBoRMaN( pts2 );
 [ lengthsBoRMaN ] = lengthsUsingBoRMaN( pts2 );
 
 
 for i=1:size(ratiosBoRMaN,1)
     BoRMaNFeatures(c,1)=ratiosBoRMaN(i,1);
     c=c+1;
 end
 
 for i=1:size(areasBoRMaN,1)
     BoRMaNFeatures(c,1)=areasBoRMaN(i,1);
     c=c+1;
 end
 for i=1:size(anglesBoRMaN,1)
    BoRMaNFeatures(c,1)=anglesBoRMaN(i,1);
     c=c+1;
 end
for i=1:size(lengthsBoRMaN,1)
     BoRMaNFeatures(c,1)=lengthsBoRMaN(i,1);
     c=c+1;
end
end


