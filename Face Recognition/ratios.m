function [rats] = ratios( pts )

rats=zeros(4,1);

 [L1] = distanceCal(pts(1),pts(2));
 [L2] = distanceCal(pts(3),pts(4));  

 [LE1] = distanceCal(pts(5),pts(6));
 [LE2] = distanceCal(pts(7),pts(8)); 

 [RE1] = distanceCal(pts(9),pts(10));
 [RE2] = distanceCal(pts(11),pts(12));  

 [N1] = distanceCal(pts(13),pts(14));
 [N2] = distanceCal(pts(15),pts(16)); 

  [ lRatio ] = ratioCal( L1,L2 );
  [ LERatio ] = ratioCal( LE1,LE2 );
  [ RERatio ] = ratioCal( RE1,RE2 );
  [ NRatio ] = ratioCal( N1,N2 );
  
  
  rats(1)=lRatio;
  rats(2)=LERatio;
  rats(3)=RERatio;
  rats(4)=NRatio;
  
end

