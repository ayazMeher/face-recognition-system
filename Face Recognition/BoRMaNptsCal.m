function [ BoRMaNpts ] = BoRMaNptsCal(Pts )

BoRMaNpts(22,1).x=zeros(22,1);
BoRMaNpts(22,1).y=zeros(22,1);

      for i=1:size(Pts,1)
  
           BoRMaNpts(i).x=Pts(i,1);
           BoRMaNpts(i).y=Pts(i,2);
      end
end

