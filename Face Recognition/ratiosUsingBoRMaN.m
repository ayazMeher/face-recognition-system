function [ rats ] = ratiosUsingBoRMaN( pts )


rats=zeros(4,1);

 [L1] = distanceCal(pts(15),pts(16));
 [L2] = distanceCal(pts(17),pts(18));  

 [LE1] = distanceCal(pts(1),pts(3));
 [LE2] = distanceCal(pts(9),pts(11)); 

 [RE1] = distanceCal(pts(4),pts(2));
 [RE2] = distanceCal(pts(10),pts(12));  

 [N1] = distanceCal(pts(13),pts(14));
 [N2] = distanceCal(pts(20),pts(17)); 

  [ lRatio ] = ratioCal( L1,L2 );
  [ LERatio ] = ratioCal( LE1,LE2 );
  [ RERatio ] = ratioCal( RE1,RE2 );
  [ NRatio ] = ratioCal( N1,N2 );
  
  
  rats(1)=lRatio;
  rats(2)=LERatio;
  rats(3)=RERatio;
  rats(4)=NRatio;

end

